from django.urls import path

from . import views

app_name = 'event_organizer'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('delete/', views.delete, name="delete"),
    path('', views.index, name='index'),
]
