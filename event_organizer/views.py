from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponse
from .forms import *
from .models import *

def index (request) :
    events = Event.objects.all()
    participants = Participant.objects.all()

    ev_par = []
    for event in events :
        event_participant = []
        for participant in participants :
            if (str(participant.event) == str(event)) :
                event_participant.append(participant)
        ev_par.append((event, event_participant))   

    context = {
        "events" : ev_par,
    }
    return render(request, 'index.html', context)

def register (request) :
    event_id = request.POST.get('event_id', None)
    participant_name = request.POST.get('participant_name', None)

    event_name = request.POST.get('event_name', None)

    if(event_id and participant_name) :
        Participant.objects.create(
            name=participant_name,
            event=Event.objects.get(id=event_id)
        )
    else :
        Event.objects.create(name=event_name)

    return redirect(reverse('event_organizer:index'))

def delete (request) :
    user_id = request.POST.get('user_id', None)
    participant = Participant.objects.get(id=user_id)

    participant.delete()
    return redirect(reverse('event_organizer:index'))
