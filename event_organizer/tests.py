from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from event_organizer.models import *
import event_organizer.views

class DetailPageUnitTest(TestCase) :
    def test_story6_url_is_exist (self) :
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        
        response = Client().get('/register')
        self.assertEqual(response.status_code, 301)

    def test_story6_model_create (self) :
        event = Event.objects.create(name="event_test")

        count = Event.objects.all().count()
        self.assertEqual(count, 1)

    def test_story6_model_create2 (self) :
        event = Event.objects.create(name="event_test")
        participant = Participant.objects.create(name="participant_test", event=event)

        count = Participant.objects.all().count()
        self.assertEqual(count, 1)

    def test_story6_model_name (self):
        event = Event.objects.create(name="event_test")

        self.assertEqual(str(event),"event_test")

    def test_story6_model_name2 (self):
        event = Event.objects.create(name="event_test")
        participant = Participant.objects.create(name="participant_test", event=event)

        self.assertEqual(str(participant),"event_test - participant_test")

    def test_story6_template_used (self) :
        event = Event.objects.create(name="event_test")
        participant = Participant.objects.create(name="participant_test", event=event)

        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story6_save_a_POST_request(self):
        response = Client().post('/register/', data={
            "event_name" : "event_test"
        })

        count = Event.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code, 302)

    def test_story6_save_a_POST_request2(self):
        event = Event.objects.create(name="event_test")

        response = Client().post('/register/', data={
            "event_id" : "1",
            "participant_name" : "dummy",
        })

        count = Participant.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code, 302)

    def test_story6_view_show (self) :
        event = Event.objects.create(name="event_test")

        response = Client().post('/register/', data={
            "event_id" : "1",
            "participant_name" : "dummy",
        })

        count = Participant.objects.all().count()
        self.assertEqual(count, 1)

        response = Client().post('/delete/', data={
            "user_id" : 1,
        })

        count = Participant.objects.all().count()
        self.assertEqual(count, 0)

"""
    def test_story6_view_show_ulasan (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        ulasan = Ulasan.objects.create(
            nama_pengulas="dummy",
            isi_ulasan="bagus",
            penilaian="4",
            barang=barang
        )
        request = HttpRequest()
        response = detailpage.views.index(request, 1)
        html_response = response.content.decode('utf8')
        self.assertIn('bagus', html_response)

    def test_story6_using_no_id (self) :
        found = resolve('/detail/')
        self.assertEqual(found.func, detailpage.views.no_id)

    def test_story6_using_index (self) :
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        found = resolve('/detail/1')
        self.assertEqual(found.func, detailpage.views.index)

    def test_story6_save_a_POST_request(self):
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )
        
        response = Client().post('/detail/1', data={
            'nama':"dummy",
            'isi':"bagus",
        })

        count = Ulasan.objects.filter(barang=barang).count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code, 200)

    def test_story6_model_name(self):
        kategori = Kategori.objects.create(nama="test_kategori")

        barang = Barang.objects.create(
            nama="barang_test",
            harga=1000,
            stok=100,
            deskripsi="test__test",
            kategori=kategori,
        )

        ulasan = Ulasan.objects.create(
            nama_pengulas="dummy",
            isi_ulasan="bagus",
            penilaian="4",
            barang=barang
        )

        ulasan = Ulasan.objects.get(nama_pengulas="dummy")
        self.assertEqual(str(ulasan),"[barang_test] - dummy - (4)")

"""
